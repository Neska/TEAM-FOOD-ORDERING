# Team Food Ordering

## Migrations

Migrations in this CodeIgniter 2 application are developed by CI recomended approach.

Writing migrations is described here: https://www.codeigniter.com/userguide2/libraries/migration.html

To run migrations, needed operation is:
<code>php index.php migrate up</code>