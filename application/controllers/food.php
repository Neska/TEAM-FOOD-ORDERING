<?php

class Food extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('food_model');
        $this->load->model('restaurant_model');
        $this->load->model('user_model');
        $this->load->helper('url_helper');

        $userID = $this->session->userdata('id');
        $rowPerson = $this->user_model->get_person($userID);

        foreach ($rowPerson as $person) {
            if ($person['status'] != 'Active') {
                $this->session->unset_userdata('id');
                $this->session->unset_userdata('role');
            }

            if ($person['role'] != 'Administrator') {
                redirect(base_url() . 'frontend/home/index');
            }

            $this->session->set_userdata('status', $person['status']);
            $this->session->set_userdata('role', $person['role']);

        }

        if ($this->session->userdata('language') == null) {
            $this->session->set_userdata('language', 'english');
        } else {
            $this->session->userdata('language');
        }
    }


    public function index()
    {
        $this->load->helper('form');
        $this->load->helper('url_helper');

        $this->lang->load('content', $this->session->userdata('language'));
        $all_lang_array = $this->lang->language;

        $food = $this->food_model->get_food();

        $restaurant = $this->restaurant_model->get_restaurants();

        $this->load->view('layout/header', [
            'language' => $all_lang_array,
        ]);

        if ($this->session->userdata('id') != '') {
            $this->load->view('food/index', [
                'foods' => $food,
                'restaurants' => $restaurant
            ]);
        } else {
            redirect(base_url() . 'login/actionLogin');
        }
        $this->load->view('layout/footer');
    }

    public function show_food_for_restaurant()
    {

        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->lang->load('content', $this->session->userdata('language'));
        $all_lang_array = $this->lang->language;

        if (isset($_POST['restaurant'])) {
            $food = $this->food_model->get_foods_for_restaurants($_POST['restaurant']);

        } else {
            $food = $this->food_model->get_food();
        }

        $restaurant = $this->restaurant_model->get_restaurants();

        $this->load->view('layout/header', [
            'language' => $all_lang_array,
        ]);

        if ($this->session->userdata('id') != '') {
            $this->load->view('food/index', [
                'foods' => $food,
                'restaurants' => $restaurant,
            ]);
        } else {
            redirect(base_url() . 'login/actionLogin');
        }
        $this->load->view('layout/footer');
    }

    public function create()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('price', 'price', 'required');
        $this->form_validation->set_rules('restaurants', 'restaurants', 'required');


        $this->lang->load('content', $this->session->userdata('language'));
        $all_lang_array = $this->lang->language;
        $restaurants = $this->restaurant_model->get_restaurants();

        if ($this->form_validation->run() === FALSE) {


            $this->load->view('layout/header', [
                'language' => $all_lang_array,
            ]);
            $this->load->view('food/create', [
                'language' => $all_lang_array,
                'restaurant' => $restaurants
            ]);
            $this->load->view('layout/footer');
        } else {

            $this->food_model->set_food();
            $this->load->view('layout/header');
            redirect(base_url() . 'food/index');
            $this->load->view('layout/footer');
        }
    }


    public function edit($id)
    {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->lang->load('content', $this->session->userdata('language'));
        $all_lang_array = $this->lang->language;
        $restaurants = $this->restaurant_model->get_restaurants();
        $data = $this->food_model->get_food_by_id($id);

        if (empty($id) || empty($data)) {
            show_404();
        }

        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('price', 'price', 'required');
        $this->form_validation->set_rules('restaurants', 'restaurants', 'required');


        if ($this->form_validation->run() === FALSE) {

            $this->load->view('layout/header', [
                'language' => $all_lang_array,
            ]);

            $this->load->view('food/update', [
                'food' => $data,
                'restaurant' => $restaurants

            ]);
            $this->load->view('layout/footer', $data);

        } else {
            $this->food_model->edit_food($id);
            redirect(base_url() . 'food/index');
        }
    }

    public function delete($id)
    {
        $this->food_model->delete_food($id);
        redirect(base_url() . 'food/index');
    }
}

?>
