<?php

class Restaurant extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('restaurant_model');
        $this->load->model('city_model');
        $this->load->model('user_model');

        $this->load->helper('url_helper');

        $userID = $this->session->userdata('id');
        $rowPerson = $this->user_model->get_person($userID);

        foreach ($rowPerson as $person) {
            if ($person['status'] != 'Active') {
                $this->session->unset_userdata('id');
                $this->session->unset_userdata('role');
            }

            if ($person['role'] != 'Administrator') {
                redirect(base_url() . 'frontend/home/index');
            }

            $this->session->set_userdata('status', $person['status']);
            $this->session->set_userdata('role', $person['role']);

        }

        if ($this->session->userdata('language') == null) {
            $this->session->set_userdata('language', 'english');
        } else {
            $this->session->userdata('language');
        }
    }

    public function index()
    {

        $this->lang->load('content', $this->session->userdata('language'));

        $all_lang_array = $this->lang->language;
        $data['restaurants'] = $this->restaurant_model->get_restaurants();

        $this->load->view('layout/header', [
            'language' => $all_lang_array,
        ]);


        if ($this->session->userdata('id') != '') {
            $this->load->view('restaurant/index', $data);
        } else {
            redirect(base_url() . 'login/actionLogin');
        }
        $this->load->view('layout/footer');
    }


    public function create()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('city', 'city', 'required');
        $this->form_validation->set_rules('phone_number', 'phone_number', 'required');


        $cities = $this->city_model->get_active_cities();


        $this->lang->load('content', $this->session->userdata('language'));
        $all_lang_array = $this->lang->language;

        if ($this->form_validation->run() === FALSE) {


            $this->load->view('layout/header', [
                'language' => $all_lang_array,
            ]);
            $this->load->view('restaurant/create', [
                'language' => $all_lang_array,
                'city' => $cities,

            ]);
            $this->load->view('layout/footer');
        } else {

            $this->restaurant_model->set_restaurants();
            $this->load->view('layout/header');
            redirect(base_url() . 'restaurant/index');
            $this->load->view('layout/footer');
        }
    }

    public function edit($id)
    {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->lang->load('content', $this->session->userdata('language'));
        $all_lang_array = $this->lang->language;

        $data = $this->restaurant_model->get_restaurants_by_id($id);
        $cities = $this->city_model->get_active_cities();

        if (empty($id) || empty($data)) {
            show_404();
        }

        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('city', 'city', 'required');
        $this->form_validation->set_rules('phone_number', 'phone_number', 'required');


        if ($this->form_validation->run() === FALSE) {

            $this->load->view('layout/header', [
                'language' => $all_lang_array,
            ]);

            $this->load->view('restaurant/update', [
                'restaurant' => $data,
                'city' => $cities,
            ]);
            $this->load->view('layout/footer', $data);

        } else {
            $this->restaurant_model->edit_restaurants($id);
            redirect(base_url() . 'restaurant/index');
        }
    }

    public function delete($id)
    {
        $this->restaurant_model->delete_restaurants($id);
        redirect(base_url() . 'restaurant/index');
    }
}

?>
