<?php

class City extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('city_model');
        $this->load->model('user_model');

        $this->load->helper('url_helper');

        $userID = $this->session->userdata('id');
        $rowPerson = $this->user_model->get_person($userID);

        foreach ($rowPerson as $person) {
            if ($person['status'] != 'Active') {
                $this->session->unset_userdata('id');
                $this->session->unset_userdata('role');
            }

            if ($person['role'] != 'Administrator') {
                redirect(base_url() . 'frontend/home/index');
            }

            $this->session->set_userdata('status', $person['status']);
            $this->session->set_userdata('role', $person['role']);

        }

        if ($this->session->userdata('language') == null) {
            $this->session->set_userdata('language', 'english');
        } else {
            $this->session->userdata('language');
        }
    }

    public function index()
    {

        $this->lang->load('content', $this->session->userdata('language'));
        $all_lang_array = $this->lang->language;

        $data['cities'] = $this->city_model->get_cities();
        $this->load->view('layout/header', [
            'language' => $all_lang_array,
        ]);

        if ($this->session->userdata('id') != '') {
            $this->load->view('city/index', $data);
        } else {
            redirect(base_url() . 'login/actionLogin');
        }
        $this->load->view('layout/footer');
        $this->load->view('layout/footer');
    }


    public function create()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('status', 'status', 'required');

        $this->lang->load('content', $this->session->userdata('language'));
        $all_lang_array = $this->lang->language;

        if ($this->form_validation->run() === FALSE) {


            $this->load->view('layout/header', [
                'language' => $all_lang_array,
            ]);
            $this->load->view('city/create', [
                'language' => $all_lang_array,
            ]);
            $this->load->view('layout/footer');
        } else {

            $this->city_model->set_city();
            $this->load->view('layout/header');
            redirect(base_url() . 'city/index');
            $this->load->view('layout/footer');
        }
    }

    public function edit($id)
    {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->lang->load('content', $this->session->userdata('language'));
        $all_lang_array = $this->lang->language;

        $data['city'] = $this->city_model->get_city_by_id($id);

        if (empty($id) || empty($data['city'])) {
            show_404();
        }

        $this->form_validation->set_rules('name', 'name', 'required');
        $this->form_validation->set_rules('status', 'status', 'required');


        if ($this->form_validation->run() === FALSE) {

            $this->load->view('layout/header', [
                'language' => $all_lang_array,
            ]);

            $this->load->view('city/update', $data);
            $this->load->view('layout/footer', $data);

        } else {
            $this->city_model->edit_city($id);
            redirect(base_url() . 'city/index');
        }
    }

    public function delete($id)
    {
        if($this->city_model->delete_city($id)) {
            redirect(base_url() . 'city/index');
        }else{
            show_404();
        }
    }
}

?>
