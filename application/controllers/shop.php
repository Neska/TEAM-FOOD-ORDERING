<?php

/**
 * @property  cart
 */
class Shop extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('cart');
        $this->load->helper('form');
        $this->load->helper('url_helper');
        $this->load->model('food_model');
        $this->load->model('user_model');
        $this->load->model('shop_model');

        $userID = $this->session->userdata('id');
        $rowPerson = $this->user_model->get_person($userID);

        foreach ($rowPerson as $person) {
            if ($person['status'] != 'Active') {
                $this->session->unset_userdata('id');
                $this->session->unset_userdata('role');
            }

            if ($person['role'] != 'Administrator') {
                redirect(base_url() . 'frontend/home/index');
            }

            $this->session->set_userdata('status', $person['status']);
            $this->session->set_userdata('role', $person['role']);

        }

        if ($this->session->userdata('language') == null) {
            $this->session->set_userdata('language', 'english');
        } else {
            $this->session->userdata('language');
        }
    }

    function view()
    {
        $this->lang->load('content', $this->session->userdata('language'));
        $all_lang_array = $this->lang->language;

        $dataToBeAddedToCart = $this->food_model->get_food();
        $this->load->view('frontend/layout/header', [
            'language' => $all_lang_array,
        ]);
        if ($this->session->userdata('id') != '') {

            $this->load->view('frontend/shop/index', [
                'foods' => $dataToBeAddedToCart,

            ]);
        } else {
            redirect(base_url() . 'login/actionLogin');

        }

        $this->load->view('frontend/layout/footer');
    }

    public function shopView()
    {

        $this->lang->load('content', $this->session->userdata('language'));
        $all_lang_array = $this->lang->language;


        if (isset($_POST['delete'])) {
            $data = [
                'rowid' => $_POST['rowid'],
                'qty' => 0
            ];

            $this->cart->update($data);
        }

        if (isset($_POST['cart'])) {

            $dataToBeAddedToCart = $this->food_model->get_food_for_card($_POST['id']);
            foreach ($dataToBeAddedToCart as $data) {

                $ItemForCart = [
                    'id' => $_POST['id'],
                    'name' => $data['name'],
                    'qty' => $_POST['qty'],
                    'price' => $data['price']
                ];

                $this->cart->insert($ItemForCart);

            }

            $this->load->view('frontend/layout/header', [
                'language' => $all_lang_array,
            ]);

            $this->load->view('frontend/shop/cart', [
                'foods' => $dataToBeAddedToCart,

            ]);

            $this->load->view('frontend/layout/footer');
        } else {

            $this->load->view('frontend/layout/header', [
                'language' => $all_lang_array,
            ]);

            $this->load->view('frontend/shop/cart');

            $this->load->view('frontend/layout/footer');

        }
    }

    public function ordered(){

     //   var_dump($this->cart->contents());

        $this->lang->load('content', $this->session->userdata('language'));
        $all_lang_array = $this->lang->language;

        $userLogged = $this->user_model->get_name_of_person_by_id($this->session->userdata('id'));

        $this->shop_model->shop($this->cart->contents(),$userLogged,$this->cart->total());

        $this->load->view('frontend/layout/header', [
            'language' => $all_lang_array,
        ]);

        $this->load->view('frontend/shop/cart');

        $this->load->view('frontend/layout/footer');
    }

    public function delete()
    {
        $this->cart->destroy();
        redirect(base_url() . 'shop/shopView');

    }
}

?>

