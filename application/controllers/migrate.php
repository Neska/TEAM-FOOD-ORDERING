<?php

/**
 * Class Migrate
 */
class Migrate extends CI_Controller {
    public function up() {
        $this->load->library('migration');

        if (!$this->migration->current()) {
            show_error($this->migration->error_string());
        }
    }
}