<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('login_model');
        $this->load->model('user_model');
        $this->load->helper('url_helper');
        $this->load->helper('form');
        $this->load->library('form_validation');

        if ($this->session->userdata('language') == null) {
            $this->session->set_userdata('language', 'english');
        }


    }

    function actionLogin($lang = '')
    {
        if ($this->session->userdata('id') !== false) {
            if ($this->session->userdata('role') === 'Administrator') {
                redirect(base_url() . 'administrator/dashboard');
            } else if ($this->session->userdata('role') === 'User') {
                // Fixme: Once when user profile page exists, navigate there.
                redirect(base_url());
            } else {
                redirect(base_url());
            }
        }

        if ($this->session->userdata('language') == null) {
            $this->session->set_userdata('language', 'english');
        } else if ($lang != '') {
            $this->session->set_userdata('language', $lang);
        }

        $this->lang->load('content', $this->session->userdata('language'));
        $all_lang_array = $this->lang->language;

        

        $this->load->view('frontend/layout/header', [
            'language' => $all_lang_array,
        ]);
        $this->load->view('index');
        $this->load->view('frontend/login/login', [
            'language' => $all_lang_array,
        ]);

        $this->load->view('frontend/layout/footer');

    }

    function login_validation()
    {

        $this->form_validation->set_rules('email', 'email', 'required');
        $this->form_validation->set_rules('password_hash', 'password_hash', 'required');

        if ($this->form_validation->run()) {
            $email = $this->input->post('email');

            if ($this->login_model->can_login($email, $this->input->post('password_hash'))) {
                redirect(base_url() . 'administrator/dashboard');

            } else {
                $this->session->set_flashdata('error', 'Invalid Username and Password');
                redirect(base_url() . 'login/actionLogin');
            }
        } else {
            redirect(base_url() . 'login/actionLogin');
        }
    }

    function forgotPassword()
    {

        $this->lang->load('content', $this->session->userdata('language'));
        $all_lang_array = $this->lang->language;

        $this->form_validation->set_rules('email', 'email', 'required');


        $email = $this->input->post('email');

        $findemail = $this->user_model->forgotPassword($email);


        if ($this->form_validation->run() === FALSE) {
            $this->load->view('frontend/layout/header', [
                'language' => $all_lang_array,
            ]);
            $this->load->view('frontend/forgotPassword');
            $this->load->view('frontend/layout/footer');

        } else {

            if ($findemail) {

                $this->user_model->sendEmail($findemail);

            } else {
                echo "<script>alert(' $email  not found, please enter correct email')</script>";
                $this->load->view('frontend/layout/header', [
                    'language' => $all_lang_array,
                ]);
                $this->load->view('frontend/forgotPassword');
                $this->load->view('frontend/layout/footer');
            }
        }
    }

    public function newPassword($secret_code)
    {
        $time = "";

        $this->lang->load('content', $this->session->userdata('language'));
        $all_lang_array = $this->lang->language;

        $userForChangePasswords = $this->user_model->get_user_by_secret_code($secret_code);

        if ($userForChangePasswords) {

            foreach ($userForChangePasswords as $user) {
                $time = $user['time'];
            }

            $date1 = date_create($time);
            $date2 = date_create(date('Y-m-d H:i:s'));
            $diff = date_diff($date1, $date2);
            if ($diff->format("%d Days") <= 1) {

                $this->form_validation->set_rules('password_hash', 'Password', 'required');
                $this->form_validation->set_rules('password_hash2', 'Password', 'required');

                if ($this->form_validation->run() === FALSE) {

                    $this->load->view('frontend/layout/header', [
                        'language' => $all_lang_array,
                    ]);
                    $this->load->view('frontend/changePassword');
                    $this->load->view('frontend/layout/footer');
                } else if ($this->input->post('password_hash') != $this->input->post('password_hash2')) {
                    $this->load->view('frontend/layout/header', [
                        'language' => $all_lang_array,
                    ]);
                    $this->load->view('frontend/changePassword', [
                        'language' => $all_lang_array,
                    ]);
                    $this->load->view('frontend/layout/footer');

                } else {
                    $this->load->view('frontend/layout/header', [
                        'language' => $all_lang_array,
                    ]);
                    foreach ($userForChangePasswords as $id) {
                        $userID = $id['user_id'];
                        $newPassword = password_hash($this->input->post('password_hash'), PASSWORD_BCRYPT);
                        $this->user_model->ForgotPasswordISChanged($userID, $newPassword);
                    }

                    redirect(base_url() . 'login/actionLogin');
                    $this->load->view('frontend/layout/footer');
                }
            } else {
                $this->load->view('frontend/layout/header', [
                    'language' => $all_lang_array,
                ]);
                $this->load->view('frontend/forgotPassword');
                $this->load->view('frontend/layout/footer');
            }
        } else {
            redirect(base_url() . 'login/actionLogin');
        }


    }


    function logout()
    {
        $this->session->unset_userdata('id');
        $this->session->unset_userdata('role');
        redirect(base_url() . 'login/actionLogin');
    }


}