<?php

/**
 * Class Migrate
 */
class HomePage extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url_helper');

        $userID = $this->session->userdata('id');
        $rowPerson = $this->user_model->get_person($userID);

        foreach ($rowPerson as $person) {
            if ($person['status'] != 'Active') {
                $this->session->unset_userdata('id');
                $this->session->unset_userdata('role');
            }

            if ($person['role'] != 'Administrator') {
                redirect(base_url() . 'frontend/home/index');
            }

            $this->session->set_userdata('status', $person['status']);
            $this->session->set_userdata('role', $person['role']);

        }
    }

    public function index(){
        $this->load->view('homepage/welcome');

    }

    function logout()
    {
        $this->session->unset_userdata('id');
        redirect(base_url() . 'login/actionLogin');
    }
}
