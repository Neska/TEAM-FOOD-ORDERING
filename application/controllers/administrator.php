
<?php

class Administrator extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->helper('url_helper');

        $userID = $this->session->userdata('id');
        $rowPerson = $this->user_model->get_person($userID);

        foreach ($rowPerson as $person) {
            if ($person['status'] != 'Active') {
                $this->session->unset_userdata('id');
                $this->session->unset_userdata('role');
            }

            if ($person['role'] != 'Administrator') {
                redirect(base_url() . 'frontend/home/index');
            }

            $this->session->set_userdata('status', $person['status']);
            $this->session->set_userdata('role', $person['role']);

        }

        if ($this->session->userdata('language') == null) {
            $this->session->set_userdata('language', 'english');
        }
    }

    public function index()
    {
        $this->lang->load('content', $this->session->userdata('language'));
        $all_lang_array = $this->lang->language;

        $data['persons'] = $this->user_model->get_persons();
        $this->load->view('layout/header', [
            'language' => $all_lang_array,
        ]);

        if ($this->session->userdata('id') != '') {
            $this->load->view('administrator/index', $data
            );
        } else {
            redirect(base_url() . 'login/actionLogin');
        }
        $this->load->view('layout/footer');
    }

    public function dashboard()
    {
        $this->lang->load('content', $this->session->userdata('language'));
        $all_lang_array = $this->lang->language;

        $this->load->view('layout/header', [
            'language' => $all_lang_array,
        ]);
        if ($this->session->userdata('id') != '') {
            $this->load->view('administrator/dashboard', [
                'language' => $all_lang_array,
            ]);

        } else {
            redirect(base_url() . 'login/actionLogin');
        }
        $this->load->view('layout/footer');
    }

    public function create()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('first_name', 'first_name', 'required');
        $this->form_validation->set_rules('last_name', 'last_name', 'required');
        $this->form_validation->set_rules('email', 'email', 'required');
        $this->form_validation->set_rules('status', 'status', 'required');
        $this->form_validation->set_rules('role', 'role', 'required');

        $this->lang->load('content', $this->session->userdata('language'));
        $all_lang_array = $this->lang->language;

        if ($this->form_validation->run() === FALSE) {

            $this->load->view('layout/header', [
                'language' => $all_lang_array,
            ]);

            $this->load->view('administrator/create', [
                'language' => $all_lang_array,
            ]);
            $this->load->view('layout/footer');
        } else {

            $persons['persons'] = $this->user_model->set_person();
            $this->load->view('layout/header');
            redirect(base_url() . 'administrator/index');
            $this->load->view('layout/footer');
        }
    }

    public function edit($id)
    {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->lang->load('content', $this->session->userdata('language'));
        $all_lang_array = $this->lang->language;

        $data['person'] = $this->user_model->get_person_by_id($id);

        if (empty($id) || empty($data['person'])) {
            show_404();
        }

        $this->form_validation->set_rules('first_name', 'first_name', 'required');
        $this->form_validation->set_rules('last_name', 'last_name', 'required');
        $this->form_validation->set_rules('email', 'email', 'required');
        $this->form_validation->set_rules('status', 'status', 'required');
        $this->form_validation->set_rules('role', 'role', 'required');

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('layout/header', [
                'language' => $all_lang_array,
            ]);

            $this->load->view('administrator/update', $data);
            $this->load->view('layout/footer', $data);
        } else {
            $this->user_model->edit_person($id);
            redirect(base_url() . 'administrator/index');
        }
    }

    public function updatePassword($id)
    {
        $this->load->helper('form');
        $this->load->library('form_validation');


        $this->lang->load('content', $this->session->userdata('language'));
        $all_lang_array = $this->lang->language;

        $data['person'] = $this->user_model->get_person_by_id($id);

        if (empty($id) || empty($data['person'])) {
            show_404();
        }

        $this->form_validation->set_rules('password_hash', 'Password', 'required');

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('layout/header', [
                'language' => $all_lang_array,
            ]);
            $this->load->view('administrator/password', $data);
            $this->load->view('layout/footer', $data);
        } else {

            $this->user_model->update_password($id);
            redirect(base_url() . 'administrator/edit/' . $id);
        }
    }

    public function delete($id)
    {
        $this->user_model->delete_person($id);
        redirect(base_url() . 'administrator/index');
    }
}

?>
