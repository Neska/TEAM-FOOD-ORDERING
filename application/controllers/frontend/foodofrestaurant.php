<?php

class Foodofrestaurant extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url_helper');
        $this->load->model('food_model');
        $this->load->model('restaurant_model');

        if ($this->session->userdata('language') == null) {
            $this->session->set_userdata('language', 'english');
        }
    }

    public function index($id)
    {
        $this->lang->load('content', $this->session->userdata('language'));
        $all_lang_array = $this->lang->language;

        if (empty($id)) {
            show_404();
        }
        $restaurant = $this->restaurant_model->get_name_of_restaurant($id);
        $foods = $this->food_model->get_foods_for_restaurants($id);

        $this->load->view('frontend/layout/header', [
            'language' => $all_lang_array,
        ]);

        $this->load->view('frontend/foodForRestaurant/index', [
            'food' => $foods,
            'restaurants' => $restaurant,
            'language' => $all_lang_array,

        ]);
        $this->load->view('frontend/layout/footer');
    }
}

?>
