<?php

class About extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url_helper');

        if ($this->session->userdata('language') == null) {
            $this->session->set_userdata('language', 'english');
        }

    }

    public function index()
    {
        $this->lang->load('content', $this->session->userdata('language'));
        $all_lang_array = $this->lang->language;

        $this->load->view('frontend/layout/header', [
            'language' => $all_lang_array,
        ]);

        $this->load->view('frontend/about/index');
        $this->load->view('frontend/layout/footer');
    }


}

?>
