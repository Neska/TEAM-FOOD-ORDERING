<?php

class Register extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_model');
        $this->load->helper('url_helper');
        $this->load->helper('form');
        $this->load->library('form_validation');

        if ($this->session->userdata('language') == null) {
            $this->session->set_userdata('language', 'english');
        }

    }

    public function create()
    {

        $this->load->helper('form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('first_name', 'first_name', 'required');
        $this->form_validation->set_rules('last_name', 'last_name', 'required');
        $this->form_validation->set_rules('email', 'email', 'required');

        $this->form_validation->set_rules('password_hash', 'password_hash', 'required');
        $this->form_validation->set_rules('password_hash2', 'password_hash2', 'required');

        $this->lang->load('content', $this->session->userdata('language'));
        $all_lang_array = $this->lang->language;


        if ($this->form_validation->run() === FALSE) {

            $this->load->view('frontend/layout/header', [
                'language' => $all_lang_array,
            ]);

            $this->load->view('frontend/register/create', [
                'language' => $all_lang_array,
            ]);
            $this->load->view('frontend/layout/footer');
        } else {

            if ($this->input->post('password_hash') == $this->input->post('password_hash2')) {

                $this->user_model->registration_for_user();

                $email = $this->input->post('email');
                $sendEmailUsersForRegistration = $this->user_model->registration($email);


                $this->load->view('frontend/layout/header');
                redirect(base_url() . 'frontend/home/index');
                $this->load->view('frontend/layout/footer');
            } else {

                $this->load->view('frontend/layout/header', [
                    'language' => $all_lang_array,
                ]);

                $this->load->view('frontend/register/create', [
                    'language' => $all_lang_array,
                ]);
                $this->load->view('frontend/layout/footer');
            }
        }
    }

    public function confirmationUser($secret_code)
    {

        $userForRegisterAccount = $this->user_model->check_user_by_secret_code($secret_code);

        if ($userForRegisterAccount) {
            foreach ($userForRegisterAccount as $user) {
                $currentUser = $user['user_id'];
            }

            $this->user_model->change_verification_of_user($currentUser);
            redirect(base_url() . 'login/actionLogin');


        } else {

            redirect(base_url() . 'frontend/register/create');
        }
    }

    public function index()
    {
        $this->lang->load('content', $this->session->userdata('language'));
        $all_lang_array = $this->lang->language;

        $this->load->view('frontend/layout/header', [
            'language' => $all_lang_array,
        ]);
        $this->load->view('frontend/register/index');
        $this->load->view('frontend/layout/footer');
    }


}

?>
