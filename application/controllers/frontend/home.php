<?php

class Home extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url_helper');
        $this->load->model('restaurant_model');
        $this->load->model('city_model');

        if ($this->session->userdata('language') == null) {
            $this->session->set_userdata('language', 'english');
        }

    }

    public function index()
    {
        $this->lang->load('content', $this->session->userdata('language'));
        $all_lang_array = $this->lang->language;

        $restaurant = $this->restaurant_model->get_restaurants();

        $this->load->view('frontend/layout/header', [
            'language' => $all_lang_array,
        ]);

        $this->load->view('frontend/home/index', [
            'restaurants' => $restaurant
        ]);
        $this->load->view('frontend/layout/footer');
    }
}

?>
