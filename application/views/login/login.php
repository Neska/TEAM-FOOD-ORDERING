<?php echo validation_errors() ?>
<h1><?= $language['login'] ?> </h1>
<div class="table-responsive">
    <?php echo form_open('login/login_validation') ?>
    <div class="create">
        <div class="col-md-5">
            <table class="table table-hover" align="center" width="20px">
                <tr>
                    <td><label for="email"><?= $language['email']; ?></label></td>
                    <td><input type="text" id="email" name="email" placeholder=<?= $language['email']; ?>><br/></td>
                </tr>

                <tr>
                    <td><label for="password_hash"><?= $language['password_hash']; ?></label></td>
                    <td><input type="password" id="password_hash" name="password_hash"
                               placeholder=<?= $language['password_hash']; ?>><br/></td>
                </tr>

            </table>
            <input type="submit" value="<?= $language['submit']; ?>">
            <?php echo '<label class="text-danger">' . $this->session->flashdata("error") . '</label>'; ?>
        </div>
    </div>
    </form>
</div>