<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="<?= base_url() ?>css/language.css"/>
</head>
<body>
<select class="language" onchange="javascript:window.location.href='<?php echo base_url(); ?>login/actionLogin/'+this.value;">
    <option value="english" <?php if($this->session->userdata('language') == 'english') echo 'selected="selected"'; ?>>English</option>
    <option value="bosnian" <?php if($this->session->userdata('language') == 'bosnian') echo 'selected="selected"'; ?>>Bosnian</option>
</select>
<br><br>

</body>
</html>
