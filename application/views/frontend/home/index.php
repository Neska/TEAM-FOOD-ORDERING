<h1 style="text-align: center">Team Food Ordering App</h1>

<?php foreach ($restaurants

as $restaurant): ?>
<?php $cities = $this->city_model->get_name_of_city($restaurant['city_id']) ?>

<div class="row">
    <div class="col-md-4">

        <div class="box">
            <div class="row">
                <div class="col-sm-2 glyphicon">
                    <p><span class="glyphicon glyphicon-cutlery"></span></p>
                </div>
                <div class="col-md-pull-3">

                    <p>
                        <a href="<?php echo base_url('/frontend/foodofrestaurant/index/' . $restaurant['id']); ?>"><?= $restaurant['name']; ?></a>
                    </p>

                </div>
            </div>

            <div class="row">
                <div class="col-sm-2 earphone">
                    <p><span class="glyphicon glyphicon-earphone"></span></p>
                </div>
                <div class="col-sm-9">
                    <p><?= $restaurant['phone_number']; ?> </p>

                </div>
            </div>

            <div class="row" style="margin-bottom: 20px;">
                <div class="col-sm-2"></div>
                <div class="col-sm-9"></div>
            </div>

            <div class="row">
                <div class="col-sm-2">
                    <p><span class=" glyphicon glyphicon-home"></span></p>
                </div>
                <div class="col-sm-6">
                    <?php foreach ($cities as $city) ?>
                    <p><?= $city['name']; ?> </p>
                </div>
            </div>

        </div>

        <div class="col-md-4">

        </div>

    </div>
    <?php endforeach; ?>
