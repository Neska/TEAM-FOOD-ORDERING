<style>
    #table td {
        padding: 60px;
    }
</style>


<br>
<table cellpadding="4" cellspacing="1" style="width:100%">

    <tr>
        <th><?= $language['qty'] ?></th>
        <th style="text-align:right"><?= $language['itemDescription'] ?></th>
        <th style="text-align:right"><?= $language['itemPrice'] ?></th>
        <th style="text-align:right"><?= $language['subTotal'] ?></th>
        <th style="text-align:right"><?= $language['RemoveFromTheCart'] ?></th>

    </tr>

    <?php foreach ($this->cart->contents() as $items): ?>
        <?= form_open('shop/shopView'); ?>
        <?php echo form_hidden('rowid', $items['rowid']); ?>

        <tr>
            <td style="margin-right:10px;"><?php echo form_input(array('name' => 'qty', 'value' => $items['qty'], 'maxlength' => '3', 'size' => '1')); ?></td>

            <td style="text-align:right"><?php echo $items['name']; ?></td>
            <td style="text-align:right"><?php echo $this->cart->format_number($items['price']); ?></td>
            <td style="text-align:right">$<?php echo $this->cart->format_number($items['subtotal']); ?></td>
            <td style="text-align:right"><input type="submit" name="delete" value="<?=$language['delete'] ?>"</td>
        </tr>
        <?php echo '</form>' ?>


    <?php endforeach; ?>

    <tr>
        <td colspan="2"> </td>
        <td class="right"><h3><strong>Total</strong></h3></td>
        <td class="right"><h3><strong>$<?php echo $this->cart->format_number($this->cart->total()); ?></strong></h3>
        </td>
        <td style="text-align:right"><a href=" <?php echo site_url('shop/delete'); ?>"><?=$language['emptyCart'] ?></a></td>
    </tr>

</table>
<strong><a href=" <?php echo site_url('shop/view'); ?>"><?=$language['shop'] ?></a></strong>
<strong><a href=" <?php echo site_url('shop/ordered'); ?>">Ordered</a></strong>


