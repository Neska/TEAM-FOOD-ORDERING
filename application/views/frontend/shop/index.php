<h1><?=$language['shop'] ?></h1>
<style>
    #table td {
        padding: 80px;
    }

</style>
<?php
$total = 0;
?>

<div class="col-md-10 " id="table" ;>
    <table cellspacing="100" cellpadding="100px">

        <tr class="active">
            <td><h3><strong><?=$language['ID'] ?></strong></h3></td>
            <td><h3><strong><?=$language['name'] ?></strong></h3></td>
            <td><h3><strong><?=$language['price'] ?></strong></h3></td>
            <td><h3><strong><?=$language['qty'] ?></strong></h3></td>
            <td><h3><strong><?=$language['buy'] ?></strong></h3></td>
        </tr>

        <?php foreach ($foods as $food): ?>
            <?= form_open('shop/shopView'); ?>
            <tr>
                <td><h3><?= $food['id'] ?></h3></td>
                <td><h3><?= $food['name'] ?></h3></td>
                <td><h3><?= $food['price'] ?></h3></td>
                <td><h3><input type="number" name="qty" value="1" style="width:100%;"/></h3></td>
                <input type="hidden" name="id" value="<?= $food['id'] ?>"/>
                <td><input type="submit" value="Add to cart" name="cart"/></td>

                <?php $total += $food['price']; ?>
            </tr>
            </form>
        <?php endforeach; ?>
    </table>

</div>
<h5><strong><?=$language['total'] ?><?= $total; ?></strong></h5>
<h3><a href=" <?php echo site_url('shop/shopView'); ?>"><?=$language['cart'] ?></a></h3>
