<?php if (validation_errors()): ?>
    <div class="alert alert-danger">
        <?php echo validation_errors() ?>
    </div>
<?php endif; ?>
<style>
    input[type=password] {
        width: 100%;
        padding: 12px;
        border: 1px solid #ccc;
        border-radius: 4px;
        box-sizing: border-box;
        margin-top: 6px;
        margin-bottom: 16px;
        resize: vertical;
    }
</style>
<h1><?= $language['register'] ?></h1>
<div class="table-responsive">
    <?php echo form_open('/frontend/register/create') ?>

    <div class="container">

        <label for="first_name"><?= $language['first_name'] ?> </label>
        <input type="text" id="first_name" name="first_name"
               value="<?php if (isset($_POST['first_name'])) echo $_POST['first_name'] ?>"
               placeholder=<?= $language['first_name'] ?>><br/>


        <label for="last_name"><?= $language['last_name'] ?></label>
        <input type="text" id="last_name" name="last_name"
               value="<?php if (isset($_POST['last_name'])) echo $_POST['first_name'] ?>"
               placeholder=<?= $language['last_name'] ?>><br/>

        <label for="email"><?= $language['email'] ?> </label>
        <input type="text" id="email" name="email" value="<?php if (isset($_POST['email'])) echo $_POST['email'] ?>"
               placeholder=<?= $language['email'] ?>><br/>

        <label for="password_hash"><?= $language['password_hash'] ?></label><br>
        <input type="password" id="password"
               value="<?php if (isset($_POST['password_hash'])) echo $_POST['password_hash'] ?>" name="password_hash"
               placeholder=<?= $language['password_hash'] ?>>

        <?php if ($this->input->post('password_hash') != $this->input->post('password_hash2')): ?>

            <p style="color: #ff0000"><?= $language['isSamePassword']?></p>

        <?php endif; ?>
        <label for="password_hash"><?= $language['repeatPassword'] ?></label><br>
        <input type="password" id="password"
               value="<?php if (isset($_POST['password_hash2'])) echo $_POST['password_hash2'] ?>" name="password_hash2"
               placeholder=<?= $language['password_hash'] ?>><br/>
        </select>

        </tr>

        <input type="submit" class="registerbtn" value="<?= $language['register'] ?>"</input>
    </div>


    </form>
</div>