<?php echo validation_errors() ?>
<div class="table-responsive">
    <h1> <?= $language['changePassword']?></h1>
    <?php echo form_open('/login/forgotPassword') ?>

    <div class="col-md-5 col-lg-offset-4">
        <div class="create">
            <div class="col-md-9">
                <table class="table table-hover">
                    <tr>
                        <td><label for="email"><?= $language['email']; ?></label></td>
                        <td><input type="text" id="email" name="email"
                                   value="<?php if (isset($_POST['email'])) echo $_POST['email'] ?>"
                                   placeholder="<?= $language['email']; ?>"><br/>
                        </td>
                    </tr>

                    <tr>
                        <td><input type="submit" value="<?= $language['send']?>"></td>
                        <td>
                            <a id="btn" href="<?php echo base_url('/login/actionLogin/'); ?>"><?= $language['back'] ?></a>

                        </td>


                    </tr>
                    <?php echo '<label class="text-danger">' . $this->session->flashdata("error") . '</label>'; ?>
                </table>

            </div>
        </div>
    </div>
    </form>
</div>