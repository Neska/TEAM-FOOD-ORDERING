<?php echo validation_errors() ?>
<div class="table-responsive">
    <h1><?= $language['login'] ?></h1>
    <?php echo form_open('/login/login_validation') ?>
    <div class="col-md-5 col-lg-offset-4">
        <div class="create">
            <div class="col-md-9">
                <table class="table table-hover">
                    <tr>
                        <td><label for="email"><?= $language['email']; ?></label></td>
                        <td><input type="text" id="email" name="email" placeholder="<?= $language['email']; ?>"><br/>
                        </td>
                    </tr>

                    <tr>
                        <td><label for="password_hash"><?= $language['password_hash']; ?></label></td>
                        <td><input type="password" id="password_hash" name="password_hash"
                                   placeholder="<?= $language['password_hash']; ?>"><br/>
                        </td>
                    </tr>

                    <tr>
                        <td><input type="submit" value="<?= $language['submit']; ?>"></td>
                        <td>
                            <a id="btn" href="<?= base_url(); ?>frontend/register/create">
                                <?= $language['register'] ?>

                            </a>
                        </td>
                        <td>

                            <a  id="forgot" href="<?php echo base_url('login/forgotPassword'); ?>"><?= $language['forgotPassword'] ?></a>
                        </td>
                    </tr>
                    <?php echo '<label class="text-danger">' . $this->session->flashdata("error") . '</label>'; ?>
                </table>

            </div>
        </div>
    </div>
    </form>
</div>