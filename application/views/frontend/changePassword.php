<?php echo validation_errors() ?>

<div class="table-responsive">
    <h1> <?= $language['newPassword']?></h1>
    <?php echo form_open('login/newPassword/' . $this->uri->segment('3')) ?>
    <div class="col-md-5 col-lg-offset-4">
        <div class="create">
            <div class="col-md-9">
                <table class="table table-hover">

                    <tr>
                        <td><label for="email"><?= $language['newPassword']; ?></label></td>

                        <td>
                            <input type="password" id="password"
                                   value="<?php if (isset($_POST['password_hash'])) echo $_POST['password_hash'] ?>"
                                   name="password_hash"
                                   placeholder=<?= $language['password_hash'] ?>><br><br>
                            <?php if ($this->input->post('password_hash') != $this->input->post('password_hash2')): ?>

                                <p style="color: #ff0000"><?= $language['isSamePassword'] ?></p>

                            <?php endif; ?>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="email"><?= $language['repeatPassword']; ?></label></td>
                        <td>
                            <input type="password" id="password"
                                   value="<?php if (isset($_POST['password_hash'])) echo $_POST['password_hash'] ?>"
                                   name="password_hash2"
                                   placeholder=<?= $language['password_hash'] ?>>

                        </td>
                    </tr>


                    <tr>
                        <td><input type="submit" value="<?= $language['send']?>"></td>
                        <td>
                            <a id="btn" href="<?php echo base_url('/login/actionLogin/'); ?>"><?= $language['back'] ?></a>

                        </td>


                    </tr>
                    <?php echo '<label class="text-danger">' . $this->session->flashdata("error") . '</label>'; ?>
                </table>

            </div>
        </div>
    </div>
    </form>
</div>