<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap-theme.min.css"/>
    <link rel="stylesheet" href="<?= base_url() ?>css/frontend/header.css"/>
    <link rel="stylesheet" href="<?= base_url() ?>css/frontend/login.css"/>
    <link rel="stylesheet" href="<?= base_url() ?>css/frontend/register.css"/>

</head>
<body>
<div class="row">
    <div class="col-md-2 block1">
        <div class="row">
            <div class="col-lg-4">
                <a href="<?php echo base_url() ?>frontend/home/index"><?= $language['home']; ?></a>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4">
                <?php if ($this->session->userdata('role') == 'Administrator'): ?>
                    <a class="navbar-brand" href="<?php echo base_url() ?>administrator/dashboard"><?= $language['administration'] ?></a>
                <?php endif; ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4">
                <?php if ($this->session->userdata('id') != ''): ?>
                    <a href="<?php echo base_url() ?>shop/view"><?= $language['shop']; ?></a>
                    <a href="<?php echo base_url() ?>login/logout"><?= $language['logout']; ?></a>
                <?php endif; ?>



                <?php if ($this->session->userdata('id') == ''): ?>
                    <a href="<?php echo base_url() ?>login/actionLogin"><?= $language['login']; ?></a>

                <?php endif; ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-4 aboutUs">
                <a href="<?php echo base_url() ?>frontend/about/index"><?= $language['about']; ?></a>
            </div>
        </div>


    </div>

    <div class="col-md-10 col-md-offset-2">

