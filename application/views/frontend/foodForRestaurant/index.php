<h1 style="margin-left: 11px"><?= $language['foodOfRestaurant'] ?>
    <?php foreach ($restaurants as $restaurant): ?>
        <?= $restaurant['name'] ?>

    <?php endforeach; ?>
</h1>
<div class="table-responsive">

    <div class="col-md-8 " id="table" ;>

        <table class="table table-striped" border='2' cellpadding="12" width="20px">
            <tr class="active">
                <td><strong><?= $language['ID'] ?></strong></td>
                <td><strong><?= $language['name'] ?></strong></td>
                <td><strong><?= $language['price'] ?></strong></td>
                <td><strong><?= $language['options'] ?></strong></td>
            </tr>

            <?php foreach ($food as $f): ?>

            <tr>
                <td id="id"><?= $f['id']; ?></td>
                <td><?= $f['name']; ?></td>
                <td><?= $f['price']; ?></td>
                <td><?= $f['options']; ?></td>

                <?php endforeach; ?>
        </table>

    </div>
</div>
