<h1 style="margin-left:40px;"><?= $language['restaurants'] ?></h1>


<a href="<?= base_url(); ?>restaurant/create" id="link">
    <button class="btn btn-success" id="btn">
        <?= $language['create'] ?>
    </button>
</a>

<div class="table-responsive">

    <div class="col-md-8 " id="table" ;>


        <table class="table table-striped" border='2' cellpadding="12" width="20px">
            <tr class="active">
                <td><strong><?= $language['ID'] ?></strong></td>
                <td><strong><?= $language['name'] ?></strong></td>
                <td><strong><?= $language['city'] ?></strong></td>
                <td><strong><?= $language['phone_number'] ?></strong></td>
                <td><strong><?= $language['actions'] ?></strong></td>

            </tr>

            <?php foreach ($restaurants as $restaurant): ?>

                <?php $cities = $this->city_model->get_name_of_city($restaurant['city_id']) ?>

                <tr>
                    <td id="id"><?= $restaurant['id']; ?></td>
                    <td><?= $restaurant['name']; ?></td>

                    <?php foreach ($cities

                    as $city) ?>
                    <td><?= $city['name'] ?></td>
                    <td><?= $restaurant['phone_number']; ?></td>

                    <td id="center">
                        <a class="glyphicon glyphicon-edit"
                           href="<?php echo site_url('/restaurant/edit/' . $restaurant['id']); ?>"></a> |

                        <a class="glyphicon glyphicon-trash"
                           href="<?php echo site_url('/restaurant/delete/' . $restaurant['id']); ?>"
                           onClick="return confirm('Are you sure you want to delete?')"></a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>

    </div>
</div>
