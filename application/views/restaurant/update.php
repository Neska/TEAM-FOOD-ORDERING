<?php if (validation_errors()): ?>
    <div class="alert alert-danger">
        <?php echo validation_errors() ?>
    </div>
<?php endif; ?>
<h1><?= $language['title'] ?> </h1>
<div class="table-responsive">

    <?php echo form_open('/restaurant/edit/' . $restaurant['id']) ?>
    <div class="update">

        <div class="col-md-5">
            <table class="table table-hover">
                <tr>
                    <td><label for="name"><?= $language['name'] ?> </label></td>
                    <td><input type="text" id="name" name="name"
                               value="<?php if (isset($_POST['name'])) {
                                   echo $_POST['name'];
                               } else {
                                   echo $restaurant['name'];
                               } ?>"
                        >
                    </td>
                </tr>

                <tr>
                    <td><label for="role"><?= $language['city'] ?></label></td>
                    <td><select name="city">
                            <?php foreach ($city as $c): ?>
                                <option value="<?php echo $c['id'] ?>" <?php if ($restaurant['city_id'] == $c['id']) echo "selected"; ?>>
                                    <?php echo $c['name']; ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                        <br><br>
                </tr>

                <tr>
                    <td><label for="phone_number"><?= $language['phone_number'] ?> </label></td>
                    <td><input type="text" id="phone_number" name="phone_number"
                               value="<?php if (isset($_POST['phone_number'])) {
                                   echo $_POST['phone_number'];

                               } else {
                                   echo $restaurant['phone_number'];
                               } ?>"
                        >
                    </td>
                </tr>


            </table>
            <input name="SubmitButton" type="submit" value="Submit">

            <a id="buttonPassword" href="<?php echo site_url('restaurant/index/'); ?>">Back </a>
        </div>
    </div>
    </form>
</div>
