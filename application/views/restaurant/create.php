<?php if(validation_errors()): ?>
<div class = "alert alert-danger">
    <?php echo validation_errors() ?>
</div>
<?php endif; ?>

<h1 xmlns:border="http://www.w3.org/1999/xhtml" xmlns:border="http://www.w3.org/1999/xhtml"><?= $language['create'] ?></h1>
<div class="table-responsive">
    <?php echo form_open('/restaurant/create') ?>
    <div class="create">
        <div class="col-md-5">

            <table class="table table-hover" align="center" width="20px">
                <tr>
                    <td><label for="name"><?= $language['name'] ?> </label></td>
                    <td><input type="text" id="first_name" name="name" placeholder=<?= $language['name']  ?>><br/></td>
                </tr>
                <tr>
                    <td><label for="city"><?=$language['city'] ?></label></td>
                    <td><select name="city">
                        <?php foreach($city as $c): ?>
                            <option value="<?php echo $c['id'] ?>"><?php echo $c['name']; ?></option>;
                        <?php endforeach; ?>
                    </select></td>

                </tr>

                <tr>
                    <td><label for="phone_number"><?= $language['phone_number']  ?> </label></td>
                    <td><input type="text" id="text" name="phone_number" placeholder=<?= $language['phone_number']  ?>><br/></td>
                </tr>



            </table>
            <input type="submit" value="Submit">

            <a id="buttonPassword" href="<?php echo site_url('restaurant/index/'); ?>">Back </a>


        </div>
    </div>
    </form>
</div>
