<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="<?= base_url() ?>assets/css/bootstrap-theme.min.css"/>
    <link rel="stylesheet" href="<?= base_url() ?>css/nav.css"/>
    <link rel="stylesheet" href="<?= base_url() ?>css/tables.css"/>
    <link rel="stylesheet" href="<?= base_url() ?>css/login.css"/>
    <link rel="stylesheet" href="<?= base_url() ?>css/formValidation.css"/>
    <link rel="stylesheet" href="<?= base_url() ?>css/food.css"/>
</head>
<body>
<nav class="navbar">
    <div class="collapse navbar-collapse">
        <div class="navbar-header">

            <a class="navbar-brand" href="<?php echo base_url() ?>frontend/home/index"><?= $language['home'] ?></a>

            <a class="navbar-brand" href="<?php echo base_url() ?>administrator/dashboard"><?= $language['dashboard'] ?></a>

            <a class="navbar-brand" href="<?php echo base_url() ?>administrator/index"><?= $language['users'] ?></a>

            <a class="navbar-brand" href="<?php echo base_url() ?>restaurant/index"><?= $language['restaurants'] ?></a>

            <a class="navbar-brand" href="<?php echo base_url() ?>city/index"><?= $language['city'] ?></a>

            <a class="navbar-brand" href="<?php echo base_url() ?>food/index"><?= $language['food'] ?></a>
        </div>
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav navbar-right">


                <?php if ($this->session->userdata('id') != ''): ?>
                    <li><a class="navbar-brand" href="<?php echo base_url() ?>login/logout"><?= $language['logout'] ?></a></li>
                <?php endif; ?>

                <?php if ($this->session->userdata('id') == ''): ?>
                    <li><a class="navbar-brand" href="<?php echo base_url() ?>login/actionLogin"><?= $language['login'] ?></a></li>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</nav>