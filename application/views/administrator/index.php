<h1 style="margin-left:40px;"><?=$language['users'] ?></h1>


<a href="<?= base_url(); ?>administrator/create" id="link">
    <button class="btn btn-success" id="btn">
        <?=$language['create']?>
    </button>
</a>


<div class="table-responsive">

    <div class="col-md-8 " id="table" ;>

        <table class="table table-striped" border='2' cellpadding="12" width="20px">
            <tr class="active">
                <td><strong><?=$language['ID'] ?></strong></td>
                <td><strong><?=$language['first_name'] ?></strong></td>
                <td><strong><?=$language['last_name'] ?></strong></td>
                <td><strong><?=$language['email'] ?></strong></td>
                <td><strong><?=$language['date'] ?></strong></td>
                <td><strong><?=$language['status'] ?></strong></td>
                <td><strong><?=$language['role'] ?></strong></td>
                <td><strong><?=$language['actions'] ?></strong></td>

            </tr>

            <?php foreach ($persons as $person): ?>

                <tr>
                    <td id="id"><?= $person['id']; ?></td>
                    <td><?= $person['first_name']; ?></td>
                    <td><?= $person['last_name']; ?></td>
                    <td><?= $person['email']; ?></td>
                    <td id="center"><?= date('Y-m-d H:i:s'); ?></td>
                    <td id="center"><?= $person['status']; ?></td>
                    <td id="center"><?= $person['role']; ?></td>
                    <td id="center">
                        <a class="glyphicon glyphicon-edit"
                           href="<?php echo site_url('administrator/edit/' . $person['id']); ?>"></a> |

                        <a class="glyphicon glyphicon-trash"
                           href="<?php echo site_url('administrator/delete/' . $person['id']); ?>"
                           onClick="return confirm('Are you sure you want to delete?')"></a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>

    </div>
</div>
