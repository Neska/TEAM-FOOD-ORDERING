<?php echo validation_errors() ?>

<h1><?=$language['change_password'] ?></h1>
<div class="table-responsive">
    <?php echo form_open('/administrator/updatePassword/' . $person['id']) ?>
    <div class="update">
        <div class="col-md-5">
            <table class="table table-hover">
                <tr>
                    <td><label for="password_hash"><?=$language['change_password'] ?></label></td>
                    <td><input type="text" id="password_hash" name="password_hash"><br/></td>

                </tr>
            </table>
            <input type="submit" value="Submit">
            <a id="buttonPassword" href="<?php echo site_url('administrator/edit/' . $person['id']); ?>">Back</a>
        </div>
    </div>
    </form>
</div>
