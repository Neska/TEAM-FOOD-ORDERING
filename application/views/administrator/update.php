<?php if (validation_errors()): ?>
    <div class="alert alert-danger">
        <?php echo validation_errors() ?>
    </div>
<?php endif; ?>
<h1><?= $language['title'] ?> </h1>
<div class="table-responsive">
    <?php echo form_open('/administrator/edit/' . $person['id']) ?>
    <div class="update">
        <div class="col-md-5">
            <table class="table table-hover">
                <tr>
                    <td><label for="first_name"><?= $language['first_name'] ?> </label></td>
                    <td><input type="text" id="first_name" name="first_name"
                               value="<?php if (isset($_POST['first_name'])) {
                                   echo $_POST['first_name'];
                               } else {
                                   echo $person['first_name'];
                               } ?>"
                        >
                    </td>
                </tr>
                <tr>
                    <td><label for="last_name"><?= $language['last_name'] ?></label></td>
                    <td><input type="text" id="last_name" name="last_name"
                               value="<?php if (isset($_POST['last_name'])) {
                                   echo $_POST['last_name'];

                               } else {
                                   echo $person['last_name'];
                               } ?>"
                        >
                    </td>
                </tr>
                <tr>
                    <td><label for="email"><?= $language['email'] ?> </label></td>
                    <td><input type="text" id="email" name="email" value="<?php if (isset($_POST['email'])) {
                            echo $_POST['email'];

                        } else {
                            echo $person['email'];
                        } ?>"
                        >
                    </td>
                </tr>
                <tr>
                    <td><label for="status"><?= $language['status'] ?></label></td>
                    <td><select name="status">
                            <option value="Active" <?php if ($person['status'] == 'Active') echo "selected"; ?>>Active
                            </option>
                            <option value="Inactive" <?php if ($person['status'] == 'Inactive') echo "selected"; ?>>
                                Inactive
                            </option>
                            <br><br>
                </tr>
                <tr>
                    <td><label for="role"><?= $language['role'] ?></label></td>
                    <td><select name="role">
                            <option value="Administrator" <?php if ($person['role'] == 'Administrator') echo "selected"; ?>>
                                Administrator
                            </option>
                            <option value="User" <?php if ($person['role'] == 'User') echo "selected"; ?>>User</option>
                            <br><br>
                </tr>
            </table>
            <input name="SubmitButton" type="submit" value="Submit">
            <a id="buttonPassword"
               href="<?php echo site_url('/administrator/updatePassword/' . $person['id']); ?>"> <?= $language['update_password'] ?></a>

            <a id="buttonPassword" href="<?php echo site_url('/administrator/index/'); ?>"> <?= $language['back'] ?> </a>


        </div>
    </div>
    </form>
</div>
