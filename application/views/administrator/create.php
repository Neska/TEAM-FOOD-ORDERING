<?php if(validation_errors()): ?>
<div class = "alert alert-danger">
    <?php echo validation_errors() ?>
</div>
<?php endif; ?>

<h1 xmlns:border="http://www.w3.org/1999/xhtml" xmlns:border="http://www.w3.org/1999/xhtml"><?= $language['create'] ?></h1>
<div class="table-responsive">
    <?php echo form_open('administrator/create') ?>
    <div class="create">
        <div class="col-md-5">
            <table class="table table-hover" align="center" width="20px">
                <tr>
                    <td><label for="first_name"><?= $language['first_name'] ?> </label></td>
                    <td><input type="text" id="first_name" name="first_name" placeholder=<?= $language['first_name']  ?>><br/></td>
                </tr>
                <tr>
                    <td><label for="last_name"><?= $language['last_name']  ?></label></td>
                    <td><input type="text" id="last_name" name="last_name" placeholder=<?= $language['last_name']  ?>><br/></td>
                </tr>
                <tr>
                    <td><label for="email"><?= $language['email']  ?> </label></td>
                    <td><input type="email" id="email" name="email" placeholder=<?= $language['email']  ?>><br/></td>
                </tr>

                <tr>
                    <td><label for="password_hash"><?= $language['password_hash']  ?></label></td>
                    <td><input type="text" id="password" name="password_hash"
                               placeholder=<?= $language['password_hash']  ?>><br/></td>
                </tr>
                <tr>
                    <td><label for="status"><?=$language['status']  ?></label></td>
                    <td><select name="status">
                            <option value="Active">Active</option>
                            <option value="Inactive">Inactive</option>
                            <option value="Deleted">Deleted</option>
                            <br><br>
                </tr>
                <tr>
                    <td><label for="role"><?=$language['role']  ?></label></td>
                    <td><select name="role">
                            <option value="Administrator">Administrator</option>
                            <option value="User">User</option>

                            <br><br>
                </tr>
            </table>
            <input type="submit" value="Submit">

            <a id="buttonPassword" href="<?php echo site_url('administrator/index/'); ?>">Back </a>


        </div>
    </div>
    </form>
</div>
