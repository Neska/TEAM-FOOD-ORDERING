<?php if(validation_errors()): ?>
<div class = "alert alert-danger">
    <?php echo validation_errors() ?>
</div>
<?php endif; ?>

<h1 xmlns:border="http://www.w3.org/1999/xhtml" xmlns:border="http://www.w3.org/1999/xhtml"><?= $language['create'] ?></h1>
<div class="table-responsive">
    <?php echo form_open('city/create') ?>
    <div class="create">
        <div class="col-md-5">
            <table class="table table-hover" align="center" width="20px">
                <tr>
                    <td><label for="name"><?= $language['name'] ?> </label></td>
                    <td><input type="text" id="name" name="name" placeholder=<?= $language['name']  ?>><br/></td>
                </tr>
                <tr>
                    <td><label for="status"><?=$language['status']  ?></label></td>
                    <td><select name="status">
                            <option value="Active">Active</option>
                            <option value="Inactive">Inactive</option>
                            <option value="Deleted">Deleted</option>
                            <br><br>
                </tr>
            </table>
            <input type="submit" value="Submit">

            <a id="buttonPassword" href="<?php echo site_url('city/index/'); ?>">Back </a>


        </div>
    </div>
    </form>
</div>
