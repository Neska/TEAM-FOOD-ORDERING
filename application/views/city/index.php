<h1 style="margin-left:40px;"><?=$language['city'] ?></h1>


<a href="<?= base_url(); ?>city/create" id="link">
    <button class="btn btn-success" id="btn">
        <?=$language['create']?>
    </button>
</a>

<div class="table-responsive">

    <div class="col-md-8 " id="table" ;>

        <table class="table table-striped" border='2' cellpadding="12" width="20px">
            <tr class="active">
                <td><strong><?=$language['ID'] ?></strong></td>
                <td><strong><?=$language['name'] ?></strong></td>
                <td><strong><?=$language['status'] ?></strong></td>
                <td><strong><?=$language['actions'] ?></strong></td>


            </tr>

            <?php foreach ($cities as $city): ?>

                <tr>
                    <td id="id"><?= $city['id']; ?></td>
                    <td><?= $city['name']; ?></td>
                    <td><?= $city['status']; ?></td>


                    <td id="center">
                        <a class="glyphicon glyphicon-edit"
                           href="<?php echo site_url('city/edit/' . $city['id']); ?>"></a> |

                        <a class="glyphicon glyphicon-trash"
                           href="<?php echo site_url('city/delete/' . $city['id']); ?>"
                           onClick="return confirm('Are you sure you want to delete?')"></a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>

    </div>
</div>
