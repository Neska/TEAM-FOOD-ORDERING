<?php if (validation_errors()): ?>
    <div class="alert alert-danger">
        <?php echo validation_errors() ?>
    </div>
<?php endif; ?>
<h1><?= $language['title'] ?> </h1>
<div class="table-responsive">
    <?php echo form_open('city/edit/' . $city['id']) ?>
    <div class="update">
        <div class="col-md-5">
            <table class="table table-hover">
                <tr>
                    <td><label for="name"><?= $language['name'] ?> </label></td>
                    <td><input type="text" id="first_name" name="name"
                               value="<?php if (isset($_POST['name'])) {
                                   echo $_POST['name'];
                               } else {
                                   echo $city['name'];
                               } ?>"
                        >
                    </td>
                </tr>
                <tr>
                    <td><label for="status"><?= $language['status'] ?></label></td>
                    <td><select name="status">
                            <option value="Active" <?php if ($city['status'] == 'Active') echo "selected"; ?>>Active
                            </option>
                            <option value="Inactive" <?php if ($city['status'] == 'Inactive') echo "selected"; ?>>
                                Inactive
                            </option>
                            <br><br>
                </tr>


            </table>
            <input name="SubmitButton" type="submit" value="Submit">

            <a id="buttonPassword" href="<?php echo site_url('city/index/'); ?>">Back </a>
        </div>
    </div>
    </form>
</div>
