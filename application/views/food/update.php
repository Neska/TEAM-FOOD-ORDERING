<?php if (validation_errors()): ?>
    <div class="alert alert-danger">
        <?php echo validation_errors() ?>
    </div>
<?php endif; ?>
<h1><?= $language['title'] ?> </h1>
<div class="table-responsive">
    <?php echo form_open('/food/edit/' . $food['id']) ?>
    <div class="update">
        <div class="col-md-5">
            <table class="table table-hover">
                <tr>
                    <td><label for="name"><?= $language['name'] ?> </label></td>
                    <td><input type="text" id="name" name="name"
                               value="<?php if (isset($_POST['name'])) {
                                   echo $_POST['name'];
                               } else {
                                   echo $food['name'];
                               } ?>"
                        >
                    </td>
                </tr>

                <tr>
                    <td><label for="price"><?= $language['price'] ?> </label></td>
                    <td><input type="number" step=".01" id="price" name="price"
                               value="<?php if (isset($_POST['price'])) {
                                   echo $_POST['price'];
                               } else {
                                   echo $food['price'];
                               } ?>"
                        >
                    </td>
                </tr>

                <tr>
                    <td><label for="restaurants"><?= $language['restaurants'] ?></label></td>
                    <td><select name="restaurants">

                            <?php foreach ($restaurant as $f): ?>
                                <option value="<?php echo $f['id'] ?>" <?php if ($food['restaurant_id'] == $f['id']) echo "selected"; ?>>
                                    <?php echo $f['name']; ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                        <br><br>
                </tr>


                <tr>
                    <td><label for="options"><?= $language['options'] ?> </label></td>
                    <td><textarea rows="4" cols="50" name="options"><?php if (isset($_POST['options'])) {
                                echo $_POST['options'];
                            } else {
                                echo $food['options'];
                            } ?>

                        </textarea>
                    </td>
                </tr>


            </table>
            <input name="SubmitButton" type="submit" value="Submit">

            <a id="buttonPassword" href="<?php echo site_url('food/index/'); ?>">Back </a>
        </div>
    </div>
    </form>
</div>
