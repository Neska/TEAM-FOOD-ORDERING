<?php if(validation_errors()): ?>
<div class = "alert alert-danger">
    <?php echo validation_errors() ?>
</div>
<?php endif; ?>

<h1 xmlns:border="http://www.w3.org/1999/xhtml" xmlns:border="http://www.w3.org/1999/xhtml"><?= $language['create'] ?></h1>
<div class="table-responsive">
    <?php echo form_open('/food/create') ?>
    <div class="create">
        <div class="col-md-5">
            <table class="table table-hover" align="center" width="20px">
                <tr>
                    <td><label for="name"><?= $language['name'] ?> </label></td>
                    <td><input type="text" id="name" name="name" placeholder=<?= $language['name']  ?>><br/></td>
                </tr>
                <tr>

                <tr>
                    <td><label for="price"><?= $language['price'] ?> </label></td>
                    <td><input type="number" step=".01" id="price" name="price" placeholder=<?= $language['price']  ?>><br/></td>
                </tr>

                <tr>
                    <td><label for="options"><?= $language['options'] ?> </label></td>
                    <td><textarea rows="4" cols="50" name="options" placeholder=<?= $language['options']  ?>></textarea></td>
                </tr>


                <tr>
                    <td><label for="restaurants"><?=$language['restaurants'] ?></label></td>
                    <td><select name="restaurants">
                            <?php foreach($restaurant as $r): ?>
                                <option value="<?php echo $r['id'] ?>"><?php echo $r['name']; ?></option>;
                            <?php endforeach; ?>
                        </select></td>

                </tr>
            </table>
            <input type="submit" value="Submit">

            <a id="buttonPassword" href="<?php echo site_url('food/index/'); ?>">Back </a>


        </div>
    </div>
    </form>
</div>
