<h1 style="margin-left:40px;"><?= $language['food'] ?></h1>

<?php echo form_open('/food/show_food_for_restaurant/') ?>
<div class="restaurant">
    <label for="restaurant"><?= $language['restaurants'] ?></label>
    <select name="restaurant" id="restaurant">

        <?php foreach ($restaurants as $r): ?>
            <?php if (isset($_POST['restaurant'])): ?>
                <option value="<?php echo $r['id'] ?>" <?php if ($r['id'] == $_POST['restaurant']) echo "selected"; ?>><?php echo $r['name']; ?></option>;
            <?php endif; ?>

            <?php if (!(isset($_POST['restaurant']))): ?>
                <option value="<?php echo $r['id'] ?>"><?php echo $r['name']; ?></option>;
            <?php endif; ?>

        <?php endforeach; ?>
    </select>
    <input type="submit" value= <?= $language['search'] ?>>
</div>
</form>

<a href="<?= base_url(); ?>food/create" id="link">
    <button class="btn btn-success" id="btn">
        <?= $language['create'] ?>
    </button>
</a><br/>

<div class="table-responsive">

    <div class="col-md-8 " id="table" ;>

        <table class="table table-striped" border='2' cellpadding="12" width="20px">

            <tr class="active">
                <td><strong><?= $language['ID'] ?></strong></td>
                <td><strong><?= $language['name'] ?></strong></td>
                <td><strong><?= $language['price'] ?></strong></td>
                <td><strong><?= $language['restaurants'] ?></strong></td>
                <td><strong><?= $language['options'] ?></strong></td>
                <td><strong><?= $language['actions'] ?></strong></td>

            </tr>

            <?php foreach ($foods as $food): ?>
                <?php $restaurants = $this->restaurant_model->get_name_of_restaurant($food['restaurant_id']); ?>
                <tr>
                    <td id="id"><?= $food['id']; ?></td>
                    <td><?= $food['name']; ?></td>
                    <td><?= number_format((float)$food['price'], 2, '.', ''); ?></td>
                    <?php foreach ($restaurants as $restaurant) ?>
                    <td><?= $restaurant['name']; ?></td>
                    <td><?= $food['options']; ?></td>


                    <td id="center">
                        <a class="glyphicon glyphicon-edit"
                           href="<?php echo site_url('/food/edit/' . $food['id']); ?>"></a> |

                        <a class="glyphicon glyphicon-trash"
                           href="<?php echo site_url('/food/delete/' . $food['id']); ?>"
                           onClick="return confirm('Are you sure you want to delete?')"></a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>

    </div>
</div>
