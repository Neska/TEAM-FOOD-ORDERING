<?php

class City_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get_cities()
    {
        $query = $this->db->get('city');

        return $query->result_array();
    }

    public function get_name_of_city($id)
    {
        $query = $this->db->get_where('city', array('id' => $id));
        return $query->result_array();
    }

    public function get_active_cities()
    {
        $this->db->where('status', 'Active');
        $query = $this->db->get('city');
        return $query->result_array();
    }

    public function set_city()
    {
        $this->load->helper('url');

        $data = array(
            'name' => $this->input->post('name'),
            'status' => $this->input->post('status'),
        );
        return $this->db->insert('city', $data);
    }

    public function get_city_by_id($id)
    {
        $query = $this->db->get_where('city', array('id' => $id));
        return $query->row_array();
    }


    public function edit_city($id)
    {

        $this->load->helper('url');

        $data = array(
            'name' => $this->input->post('name'),
            'status' => $this->input->post('status'),
        );

        $this->db->where('id', $id);
        return $this->db->update('city', $data);
    }

    public function delete_city($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('city');


    }
}