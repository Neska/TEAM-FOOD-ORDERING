<?php

class User_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library('email');
        $this->load->helper('url');
    }

    public function get_persons()
    {
        $this->db->like('status', 'Active');
        $this->db->or_like('status', 'Inactive');
        $query = $this->db->get('tfo_user');
        return $query->result_array();
    }

    public function set_person()
    {
        $this->load->helper('url');

        $data = array(
            'first_name' => $this->input->post('first_name'),
            'last_name' => $this->input->post('last_name'),
            'email' => $this->input->post('email'),
            'created_at' => date('Y-m-d H:i:s'),
            'password_hash' => password_hash($this->input->post('password_hash'), PASSWORD_BCRYPT),
            'status' => $this->input->post('status'),
            'role' => $this->input->post('role'),
            'verification' => 'Verified'

        );

        return $this->db->insert('tfo_user', $data);
    }

    public function registration_for_user()
    {
        $this->load->helper('url');

        $data = array(
            'first_name' => $this->input->post('first_name'),
            'last_name' => $this->input->post('last_name'),
            'email' => $this->input->post('email'),
            'created_at' => date('Y-m-d H:i:s'),
            'password_hash' => password_hash($this->input->post('password_hash'), PASSWORD_BCRYPT),
            'status' => 'Active',
            'role' => 'User',
            'verification' => 'Unverified'

        );

        return $this->db->insert('tfo_user', $data);
    }


    public function forgotPassword($email)
    {
        $query = $this->db->get_where('tfo_user', array('email' => $email));
        return $query->result_array();
    }

    public function newPassword($email)
    {

        $newpassHash = array(
            'password_hash' => password_hash($this->input->post('password_hash'), PASSWORD_BCRYPT),
        );

        $this->db->where('email', $email);
        $this->db->update('tfo_user', $newpassHash);
    }


    public function sendEmail($data)
    {

        if (empty($data)) {
            show_404();
        }

        $secret_code = rand(7771, 22264);
        $from_email = "nesibaspahic1997@gmail.com";
        $to_email = $this->input->post('email');

        $secret = md5($secret_code);
        $msg = 'Please click on the below link to change  your password   http://tfo.test/login/newPassword/' . $secret;

        foreach ($data as $d) {

            $data = array(
                'user_id' => $d['id'],
                'secret_code' => $secret,
                'time' => date('Y-m-d H:i:s'),
            );

            $this->db->insert('forgot_password', $data);
        }
        $this->email->from($from_email, 'Nesiba');
        $this->email->to($to_email);
        $this->email->subject('Forgot Password');
        $this->email->message($msg);


        if ($this->email->send()) {

            $this->session->set_flashdata("email_sent", "Email sent successfully.");
            redirect(base_url() . 'login/actionLogin');
        } else {
            $this->session->set_flashdata("email_sent", "Error in sending Email.");
            redirect(base_url() . 'login/forgotPassword');
        }
    }

    public function ForgotPasswordISChanged($id, $newPassword)
    {
        $data = array(
            'password_hash' => password_hash($this->input->post('password_hash'), PASSWORD_BCRYPT),
        );

        $this->db->where('id', $id);
        return $this->db->update('tfo_user', $data);
    }


    public function get_user_by_secret_code($secret_code)
    {
        $query = $this->db->get_where('forgot_password', array('secret_code' => $secret_code));
        return $query->result_array();
    }

    public function check_user_by_secret_code($secret_code)
    {
        $query = $this->db->get_where('registration', array('secret_code' => $secret_code));
        return $query->result_array();
    }

    public function change_verification_of_user($id){

        $this->db->where('id', $id);

        $row = array('verification' => 'Verified');
        return $this->db->update('tfo_user', $row);
    }


    public function registration($email)
    {


        $this->load->library('email');
        $from_email = "nesibaspahic1997@gmail.com";
        $to_email = $email;
        $secret_code = rand(7771, 22264);

        $secret = md5($secret_code);
        $msg = 'Dear User, Please click on the below activation link to verify your email address  http://tfo.test/frontend/register/confirmationUser/' . $secret;

        $this->email->from($from_email, 'Nesiba');
        $this->email->to($to_email);
        $this->email->subject('REGISTER ACCOUNT');
        $this->email->message($msg);

        $query = $this->db->get_where('tfo_user', array('email' => $to_email));
        $row = $query->result_array();


        $data = array(
            'user_id' => $row[0]['id'],
            'secret_code' => $secret,
        );

        $this->db->insert('registration', $data);

        if ($this->email->send()) {

            $this->session->set_flashdata("email_sent", "Email sent successfully.");
            redirect(base_url() . 'login/actionLogin');
        } else {
            $this->session->set_flashdata("email_sent", "Error in sending Email.");
            redirect(base_url() . 'login/forgotPassword');
        }
    }


    public function get_person($id)
    {
        $query = $this->db->get_where('tfo_user', array('id' => $id));
        return $query->result_array();
    }

    public function get_person_by_id($id)
    {
        $query = $this->db->get_where('tfo_user', array('id' => $id));
        return $query->row_array();
    }

    public function get_name_of_person_by_id($id)
    {
        $query = $this->db->get_where('tfo_user', array('id' => $id));
        return $query->result_array();
    }



    public function edit_person($id)
    {

        $this->load->helper('url');


        $data = array(
            'first_name' => $this->input->post('first_name'),
            'last_name' => $this->input->post('last_name'),
            'email' => $this->input->post('email'),
            'status' => $this->input->post('status'),
            'role' => $this->input->post('role')

        );

        $this->db->where('id', $id);
        return $this->db->update('tfo_user', $data);
    }

    public function update_password($id)
    {
        $data = array(
            'password_hash' => password_hash($this->input->post('password_hash'), PASSWORD_BCRYPT),
        );

        $this->db->where('id', $id);
        return $this->db->update('tfo_user', $data);
    }

    public function delete_person($id)
    {
        $this->db->where('id', $id);

        $row = array('status' => 'Deleted');
        return $this->db->update('tfo_user', $row);


    }
}