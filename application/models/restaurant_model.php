<?php

class Restaurant_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get_restaurants()
    {

        $query = $this->db->get('restaurant');
        return $query->result_array();
    }

    public function get_name_of_restaurant($id)
    {
        $query = $this->db->get_where('restaurant', array('id' => $id));
        return $query->result_array();


    }


    public function set_restaurants()
    {
        $this->load->helper('url');

        $data = array(
            'name' => $this->input->post('name'),
            'city_id' => $this->input->post('city'),
            'phone_number' => $this->input->post('phone_number'),

        );

        return $this->db->insert('restaurant', $data);

    }

    public function get_restaurants_by_id($id)
    {
        $query = $this->db->get_where('restaurant', array('id' => $id));
        return $query->row_array();
    }


    public function edit_restaurants($id)
    {

        $this->load->helper('url');

        $data = array(
            'name' => $this->input->post('name'),
            'city_id' => $this->input->post('city'),
            'phone_number' => $this->input->post('phone_number'),
        );

        $this->db->where('id', $id);
        return $this->db->update('restaurant', $data);
    }

    public function delete_restaurants($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('restaurant');


    }
}