<?php

class Login_model extends CI_Model
{
    function can_login($email, $password)
    {
        $this->db->where('email', $email);
        $query = $this->db->get('tfo_user');
        if ($query->num_rows() > 0) {

            $row = $query->row();

            if ($row->status != 'Active') {
                return false;
            } else {
                if (password_verify($password, $row->password_hash)) {
                    $role = $row->role;
                    $this->session->set_userdata('role', $role);
                    $session_data = array(
                        'id' => $row->id,
                    );
                    $this->session->set_userdata($session_data);
                    return true;
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
    }

}
