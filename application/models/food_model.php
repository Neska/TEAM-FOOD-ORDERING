<?php

class Food_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }

    public function get_food()
    {

        $query = $this->db->get('food');
        return $query->result_array();
    }

    public function get_foods_for_restaurants($id_restaurant)
    {
        $query = $this->db->get_where('food', array('restaurant_id' => $id_restaurant));
        return $query->result_array();

    }

    public function set_food()
    {

        $this->load->helper('url');

        $data = array(
            'name' => $this->input->post('name'),
            'price' => $this->input->post('price'),
            'restaurant_id' => $this->input->post('restaurants'),
            'options' => $this->input->post('options')
        );

        return $this->db->insert('food', $data);
    }

    public function get_food_by_id($id)
    {
        $query = $this->db->get_where('food', array('id' => $id));
        return $query->row_array();
    }

    public function get_food_for_card($id)
    {
        $query = $this->db->get_where('food', array('id' => $id));
        return $query->result_array();
    }



    public function edit_food($id)
    {

        $this->load->helper('url');

        $data = array(
            'name' => $this->input->post('name'),
            'price' => $this->input->post('price'),
            'restaurant_id' => $this->input->post('restaurants'),
            'options' => $this->input->post('options')
        );

        $this->db->where('id', $id);
        return $this->db->update('food', $data);
    }


    public function delete_food($id)
    {
        $this->db->where('id', $id);
        return $this->db->delete('food');
    }
}