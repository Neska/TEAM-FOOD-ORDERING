<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_column_in_table_tfo_user extends CI_Migration {

    public function up() {

        $fields = array(
            'verification' => array('type' =>  'VARCHAR', 'constraint' => '200')
        );

        $this->dbforge->add_column('tfo_user', $fields);

    }
}


