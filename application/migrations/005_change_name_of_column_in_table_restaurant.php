<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Change_name_of_column_in_table_restaurant extends CI_Migration {

    public function up() {
        $this->dbforge->modify_column([


            $fields = array(
                'city' => array(
                    'name' => 'city_id',
                    'type' => 'INT',
                ),
            ),

        ]);
        $this->dbforge->modify_column('restaurants', $fields);
    }
}


