<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_column_in_table_forgot_password extends CI_Migration {

    public function up() {

        $fields = array(
            'time' => array('type' =>  'timestamp')
        );
        $this->dbforge->add_column('forgot_password', $fields);

    }
}


