<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Rename_table_restaurant extends CI_Migration {

    public function up() {

        $this->dbforge->rename_table('restaurants', 'restaurant');

    }
}


