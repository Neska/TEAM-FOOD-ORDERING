<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Forgot_password extends CI_Migration {

    public function up() {
        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],

            'user_id' => [
                'type' => 'int',
                'constraint' => 5,
                'unsigned' => TRUE,

            ],

            'secret_code' => [
                'type' => 'VARCHAR',
                'constraint' => 16,


            ],

        ]);

        $this->dbforge->add_key('id');
        $this->dbforge->create_table('forgot_password');
    }
}