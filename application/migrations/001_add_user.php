<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_user extends CI_Migration
{

    public function up()
    {
        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'first_name' => [
                'type' => 'VARCHAR',
                'constraint' => '60'
            ],
            'last_name' => [
                'type' => 'VARCHAR',
                'constraint' => '60'
            ],
            'email' => [
                'type' => 'VARCHAR',
                'constraint' => '60'
            ],
            'status' => [
                'type' => 'VARCHAR',
                'constraint' => '15'
            ],
            'role' => [
                'type' => 'VARCHAR',
                'constraint' => '20'
            ],
            'password_hash' => [
                'type' => 'VARCHAR',
                'constraint' => '255'
            ],

        ]);

        $this->dbforge->add_key('id');
        $this->dbforge->create_table('tfo_user');
    }
}