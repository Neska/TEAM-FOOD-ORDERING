<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Changed_constraint_of_registration extends CI_Migration {

    public function up() {
        $this->dbforge->modify_column([


            $fields = array(
                'secret_code' => array(
                    'name' => 'secret_code',
                    'type' => 'VARCHAR',
                    'constraint' => '32'

                ),
            ),

        ]);
        $this->dbforge->modify_column('registration', $fields);
    }
}


