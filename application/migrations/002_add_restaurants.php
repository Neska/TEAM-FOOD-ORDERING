<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_restaurants extends CI_Migration {

    public function up() {
        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'name' => [
                'type' => 'VARCHAR',
                'constraint' => '70'
            ],

            'city' => [
                'type' => 'INT',

            ],

            'phone_number' => [
                'type' => 'VARCHAR',
                'constraint' => '15'

            ],

        ]);

        $this->dbforge->add_key('id');
        $this->dbforge->create_table('restaurants');
    }
}