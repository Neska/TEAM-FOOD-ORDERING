<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Changed_type_of_food_price extends CI_Migration {

    public function up() {
        $this->dbforge->modify_column([


            $fields = array(
                'price' => array(
                    'name' => 'price',
                    'type' => 'DECIMAL',
                    'constraint' => '10,2'

                ),
            ),

        ]);
        $this->dbforge->modify_column('food', $fields);
    }
}


