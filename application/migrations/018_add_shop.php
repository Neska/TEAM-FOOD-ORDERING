<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_shop extends CI_Migration {

    public function up() {
        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'user' => [
                'type' => 'VARCHAR',
                'constraint' => '70'
            ],

            'order' => [
                'type' => 'VARCHAR',
                'constraint' => '70'
            ],

            'total_price' => [
                'type' => 'INT',
                'constraint' => '100'
            ],


        ]);

        $this->dbforge->add_key('id');
        $this->dbforge->create_table('shop');
    }
}