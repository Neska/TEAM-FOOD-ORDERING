<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Change_name_of_column_in_table_food extends CI_Migration {

    public function up() {
        $this->dbforge->modify_column([


            $fields = array(
                'restaurants' => array(
                    'name' => 'restaurant_id',
                    'type' => 'INT',
                ),
            ),

        ]);
        $this->dbforge->modify_column('food', $fields);
    }
}


