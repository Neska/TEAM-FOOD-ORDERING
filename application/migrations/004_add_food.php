<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_food extends CI_Migration {

    public function up() {
        $this->dbforge->add_field([
            'id' => [
                'type' => 'INT',
                'constraint' => 5,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ],
            'name' => [
                'type' => 'VARCHAR',
                'constraint' => '70'
            ],

            'price' => [
                'type' => 'INT',
                'constraint' => '100'
            ],

            'restaurants' => [
                'type' => 'INT',
                'constraint' => '70'
            ],


            'options' => [
                'type' => 'VARCHAR',
                'constraint' => '200'
            ],

        ]);

        $this->dbforge->add_key('id');
        $this->dbforge->create_table('food');
    }
}