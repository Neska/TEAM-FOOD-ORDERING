<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Remove_column_from_table_registration extends CI_Migration {

    public function up() {
        $this->dbforge->drop_column('registration', 'time');

    }
}




