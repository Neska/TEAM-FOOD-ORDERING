<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_column_in_table_shop extends CI_Migration {

    public function up() {

        $fields = array(
            'date' => array('type' =>  'timestamp')
        );
        $this->dbforge->add_column('shop', $fields);

    }
}


